NAME TBD

By Gardiner Bryant, Raven67854, and the community

Revision 0.4

2020/02/01

NOTE
====

If, after reading this document, you have ideas you think would fit the
scope of this game, please head over to [the suggestion thread](https://gitlab.com/heavyelement/foss-game/issues/18),
and share your idea!

Introduction
============

NAME TBD will feature vehicular car combat with simple, fun, arcade-y,
and refined controls. Our goal is a game which accomodates both entry
level players and also high level competetive play. (Not necessarilly in
the same game).

We aim to provide both a local- and online-multiplayer experience.

The game's aesthetic is simple and stylized. We are inspired by the car-based,
arena combat of Twisted Metal, the item pickups and general feel of Kart
racing games, and the arcade-y caputre-the-flag nature of Calling All
Cars. Also, add a dash of competetive play borrowed from Rocket League.

Game Analysis
-------------

 | Analysis            |                                                                                        |
 | ------------------: | -------------------------------------------------------------------------------------- |
 | **Description**     |                                                                                        |
 | Genre:              | Arena-based, Arcade-y Vehicular Combat                                                 |
 | Game Elements:      | Collect weapons, car combat, movement mastery, capture the flag                        |
 | Game Content:       |                                                                                        |
 | Theme:              | Stylish, inspired by Karting games                                                     |
 | Style:              | Cartoon/Animation                                                                      |
 | Player:             | Local, Online Multiplayer                                                              |
 | **Reference**       |                                                                                        |
 | Player Immersion:   | The player should enjoy the visceral thrill of combat, strategy, and growing in skill. |
 | References:         | Twisted Metal, Rocket League, Calling All Cars, Mario Kart, Crash Team Racing          |
 | **Technical**       |                                                                                                                    |
 | Aspect:             | 3D graphics                                                                                                        |
 | View:               | The camera will take a 3rd person perspective, however we should also experiment with a 3/4 top-down perspective.  |
 | Platform:           | TBD                                                                                                                |
 | Device:             | Linux/Open Source PC Operating Systems                                                                             |

Game Atmosphere
---------------
The game's atmosphere should be fun and inviting, stylistic enough to
able to withstand the inevitable aging of graphics, and belie the true
depth of the gameplay

The game's atmosphere should be bright, colorful, and fun. Think Crash
Team Racing, Mario Odessey.
![Mood Board](/Documentation/mood-board.jpg)


Gameplay
--------

### First Launch

On first launch, the player will create a profile and choose a player
name. They will recieve a quick tutorial on how to play the game.

### Choosing a character

The characters should be presented in a list similar to that of
Overwatch, with bespoke animations which lend each character some personality.

### Joining a Match

Joining a match should be simple. See [online play](#online-play).

### The Match

Multiplayer gameplay will consist of a sort-of "capture the flag" style
game loop. Players will try to collect the *macguffin* and deposit it into
one of the receptacles on the map.

There is only ONE *macguffin* at a time in any match.

When a player acquires the *macguffin*, the HUD of other players will be
alerted. This puts a target on the carriers back.

Receptacles will be goal rings which the player must pass the *macguffin*
through in order to score points. Each map will have one easy ring worth
**one point**. There shall be at at most three more goal rings,
positioned in increasingly more difficult places---but also worth more
points.

In the standard game mode, the game is played in two teams of 4-6
players each. The standard game mode will be a timed match, with the
team having the highest score at the end being the winner.

Strewn across each map are fixed spawn points for weapon crates. The
weapon crates will behave much like item pickup in Kart racers. The
player breaks the crate and they get a random item---most of the time
the item will be single-use.

Core Mechanics
==============

Controls
--------

The game should target a standard Xbox 360, DualShock 4, or Switch Pro
controller as the primary method of input. We may additionally support
keyboard and mouse, but for this type of game, a controller is considered
optimal.

### Layout

 | Button        | Action                |
 | :-----------: | ---------------       |
 | Right Trigger | Accelerate            |
 | Right Bumper  | Activate shield       |
 | Left Bumper   | Jump/slide (hold)     |
 | Left Trigger  | Use character ability |
 | B button      | Brake                 |
 | A button      | Boost                 |
 | X button      | Use Item              |
 | Left Stick    | Steer/Point shield    |
 | Right Stick   | Control camera        |

> This mapping uses an Xbox controller as the button configuration

Characters
----------

The roster of characters will be unique to this title. Each character
will have a special ability---offensive and/or defensive---much like
hero shooters in the vein of Overwatch.

Characters will also have their own stats like acceleration, drift,
weight.

Characters will be able to supplement their abilities with common
pickups found in each map.

The game will launch with 12 unique characters.

*Example characters might be:*

  | Character    | Description & Ability |
  | ----         | ---- |
  | The Captain  | A captain who drives a standard car. He has the ability to shoot cannonballs. |
  | ED150-N      | The brain of Thomas Edison preserved in a jar, driving a miniature tank. Instead of a tank cannon, he has a giant magnet which will attract the *macguffin*, and it can also rip the *macguffin* away from another nearby player. |
  | Frankentesla | A Frankenstein's Monster version of Nikola Tesla. He drives a junker hatchback sedan with a mess of weird modifications. Frankentesla has a launcher which can shoot the *macguffin* through a target. |
  | Noodles      | A cat that is also a hacker who can hack her opponent's vehicles---she can "derez" them (obscure their screen behind pixelation) ^[thanks](https://gitlab.com/heavyelement/foss-game/issues/18#note_279129893)^ |
  | Nitro Phil   | A guy who has nitro boost on his vehicle. This boost can be used at will and propel him faster than any other character. |

#### Carrier State

Each character may take on a unique state when they have collected the
*macguffin*.

For example: One character might drag the *macguffin* around behind their
vehicle using a chain and---in order to free the *macguffin* from the
carrier---another player must drive over the chain.

Or a character like ED150-N might have the *macguffin* stuck to the magnet
on the exterior of his vehicle.

We want the carrier to be vulnerable to losing the *macguffin* without the
carrier needing to be destroyed. Thusly, the unique carrier state for each
character specified here is meant to institute such a vulnerability.

#### Models

Each character will have a **character model** and a **vehicle model**.
On the character select screen, you'll see the character model standing 
outside their vehicle with an idle animation playing until you select
them.

Once in a match, it will be rare to see the character model (characters
don't get out of their vehicles except within a canned taunting animation,
etc.)

#### Vehicle Customization

Vehicles should be able to be customized in a limited fashion, including
colors, wheels, other vehicle mods, and perhaps character variants. How
much any of this customization impacts gameplay is TBD.

#### Taunting

Each character should have a limited number of taunts they can activate.

Scoring
-------

Scoring is team-based. Scoring is achieved by getting the *macguffin*
through the receptacle. Each receptacle has different point values.

If a player is carrying the *macguffin*, passing through the receptacle
counts as a goal. If a player drops or throws the *macguffin*, the player's
name is attached to it until another player acquires it.

If no other player acquires the *macguffin* before it passes through a
receptacle, then the last player to have held the *macguffin* scores for
their team.

### *Macguffin*

The *macguffin* is the object that each team is trying to aquire and
deposit into the receptacle. This *macguffin* will take the form of a ball
(about the size of a beach ball, maybe larger). Players interact with the ball by
driving into it---at which point it's in a "collected" state and travels
with the player until the player is destroyed or they otherwise lose
posession of the *macguffin*. There is only one *macguffin* in the arena at
any time.*macguffin* will take the form of a ball (about the size of a
beach ball). Players interact with the ball by driving into it---at
which point it's in a "collected" state and travels with the player
until the player is destroyed or they otherwise lose posession of the
*macguffin*. There is only one *macguffin* in the arena at any time.*macguffin*
will take the form of a ball (about the size of a beach ball). Players
interact with the ball by driving into it---at which point it's in a
"collected" state and travels with the player until the player is
destroyed or they otherwise lose posession of the *macguffin*. There is
only one *macguffin* in the arena at any time.

### Receptacles

The goal of the game is to earn points by delivering the *macguffin* to one
of the receptacles. There are no more than four receptacles on any given
map. One that's easily accessable and worth one point, with the rest
being placed in harder to reach areas and worth proportionally more
points. (Examples might be: a ring floating in mid-air, requiring a
boost off a ramp to reach)

Receptacles are rings through which the *macguffin* must be
delivered---either with the carrier driving through the ring, or by
getting the *macguffin* through the ring and being the last team to have
carried it.

Scoring goals should feel good---like scoring a goal in Rocket League.

Driving
-------

Driving your vehicle should feel good. Momentum, drifting, jumping; each
of these things should be inherently fun on their own terms.

#### Physics

Driving should be physics-based, where the properties of your character,
your current acceleration, and the terrain you're driving on impact the
simulation. This should feel both weighty, fun, and fast. Nailing down
the feel of driving should be **our number one priority.**

^[thanks](https://gitlab.com/heavyelement/foss-game/issues/18#note_279097370)^

#### **Drifting**

Drifting will allow players to make tight turns, swing their vehicle
180\* (or greater), and allow the player to build up a boost.

Using drifts to dodge incoming projectiles will grant you extra boosts.
^[thanks](https://gitlab.com/heavyelement/foss-game/issues/18#note_279230371)^

#### Bosting

As mentioned above, drifting will allow you to boost. This is done through you
**boost juice**. Boost juice is a limited fuel which gives you the ability to 
boost your vehicle in short bursts.

Boosting your vehicle can also be achieved through pickups which grant unlimited
boost juice for a limited time, boost pads, or through character-specific actions.

#### **Jumping**

Players should be able to make their vehicles jump. Doing so in
combination with other driving mechanics (jumping off a ramp, for
example) should feel natural and allow vehicles to reach receptacles or
other areas of the map.
^[thanks](https://gitlab.com/heavyelement/foss-game/issues/18#note_279561787)^

#### **Vehicle Types**

There should be a few distinct types of locomotion in the game:
* Wheeled vehicles
* Hovercraft
* Tank/treaded vehicles
* *Others tbd*

Characters will have _one_ of the above types of vehicles.

Shielding
---------

Every character will have the ability to deploy a temporary shield which
only lasts for a second or so.

You must hold down the shield trigger and use the left stick to position
the shield, as the shield will only protect about one quarter of your
vehicle at a time.

Deploying your shield is risky as there's a cooldown for using it, but
also a much longer cooldown if the shield takes damage. Deploying the
shield will also inhibit your ability to brake, accelerate, or steer
your vehicle.

Pickups
-------

Item pickups will be crates that spawn at fixed position across a map.
These items should be an array of both offensive and defensive items.

Examples of pickups are:

  | Defensive                   | Offensive |
  | --------------------------- | --------------------------------- |
  | On-demand boosts            | *macguffin* Magnet |
  | Temporary Invulnerability   | Directional/Heatseeking Rockets |

#### Collecting Items

Drive through an item crate, it will break apart and disappear. Your HUD
will light up with a slot-machine-like UI. The item you get will be
randomly selected.

The item crate will be unusable by another player until it respawns.

**Using Items**

Items will be activated using the item trigger.

Boost Pads
----------

There will be boost pads throughout each map which accelerate vehicles
in the direction they're traveling. These boost pads will be positioned
to allow a player to make jumps off of ramps

Maps
----

The game will initially have 8 maps, each map will be based on a theme.

### Terrain

There will be multiple types of terrain that will help/hinder your
ability to navigate

  | Dirt/short grass    | The most navigable terrain |
  | ------------------- | ---------------------------------------------------------------------------------------------------- |
  | Sand/Tall grass     | Slightly slowed movement for most vehicle types, sand should also be somewhat slippery to drive on |
  | Shallow water/mud   | Very slow movement |
  | Ice/oil slick       | Slippery movement, inability to effectively steer |

### Environments

Maps will be richly detailed environments---but said detail should not
negatively impact a players ability to read the arena.

There should be various types of environments/themes including:

-   Desert
-   Forest
-   City
-   Snow/ice

### Hazards

There will be hazards on each map that will impact the gameplay. Common
hazards will include:

-   Deep water
-   Bottomless pits
-   Stage-specific hazards

### Damage

As your vehicle takes damage, it should go through visual stages to
represent its current HP. The overall health of your vehicle can be
determined by the state the camera is in; the camera, its positioning,
and severity of chomatic abberation could be indicataors. ^[thanks](https://gitlab.com/heavyelement/foss-game/issues/18#note_279147156)^

Game Modes
==========

Local Play
----------

Local play will allow for split-screen gameplay on a local machine. Yes,
split-screen gameplay on a PC. Deal with it.

Local play will allow up to four split screen players to play online in
an online match.

Online Play
-----------

### Matchmaking

Joining a match should be simple. We should have an official server that
allows users to opt-in to gameplay metrics tracking for use in pairing
you with other players of your skill level.

Stats like K/D ratio, average points scored per match, and an overall 
leaderboad/ranking system will be maintained on the official server and
will be used to put simiarly-skilled players in matches with each other.

This will be an opt-in feature, but probably required for matchmaking.

### Custom Game Lobbies

Unranked games, custom games types, etc. will be available in the client
for users who do not wish to participate in matchmaking.

### Servers

The game is open source and, as such, anyone will be able to spin up their
own third-party server and host unranked matches. The game client should
accomodate this.



Game Options
============

-   Turn off stage hazards
